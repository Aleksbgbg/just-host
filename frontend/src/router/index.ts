import { createRouter, createWebHistory } from "vue-router";

export const router = createRouter({
  history: createWebHistory(),
  routes: [{ name: "home", path: "/", component: () => import("@/views/home.vue") }],
});
