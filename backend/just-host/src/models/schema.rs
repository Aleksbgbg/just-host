// @generated automatically by Diesel CLI.

diesel::table! {
    users (id) {
        id -> Int8,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        username -> Text,
        email -> Text,
        password_hash -> Text,
        auth_secret -> Bytea,
    }
}
